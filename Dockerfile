FROM amazonlinux:2023

RUN yum install https://rpm.nodesource.com/pub_20.x/nodistro/repo/nodesource-release-nodistro-1.noarch.rpm -y && \
    yum install aws-cli git tar nodejs -y --setopt=nodesource-nodejs.module_hotfixes=1 && \
    mkdir -p smithy-install/smithy && \
    curl -L https://github.com/smithy-lang/smithy/releases/download/1.42.0/smithy-cli-linux-x86_64.tar.gz -o smithy-install/smithy-cli-linux-x86_64.tar.gz && \
    tar xvzf smithy-install/smithy-cli-linux-x86_64.tar.gz -C smithy-install/smithy && \
    smithy-install/smithy/install